import requests
import json


def test_create_user(supply_url):
    url = supply_url + "/users"
    resp = requests.post(
        url, json={"name": "Joseba", "surname": "Carnicero", "address": "Bergara"}
    )
    assert resp.status_code == 201
    print(resp.text)
    data = json.loads(resp.text)
    print(data)
    assert "User Joseba was added!" in data["message"]


def test_single_user(supply_url):
    url = supply_url + "/users/1"
    resp = requests.get(url)
    assert resp.status_code == 200
    data = json.loads(resp.text)
    assert "Joseba" in data["name"]
    assert "Carnicero" in data["surname"]
    assert "Bergara" in data["address"]


def test_create_order(supply_url):
    url = supply_url + "/orders"
    resp = requests.post(
        url, json={"description": "Order1", "number_of_pieces": 2, "user_id": 1}
    )
    assert resp.status_code == 201
    print(resp.text)
    data = json.loads(resp.text)
    print(data)
    assert "Order 1 was added!" in data["message"]


def test_single_order(supply_url):
    url = supply_url + "/orders/1"
    resp = requests.get(url)
    assert resp.status_code == 200
    data = json.loads(resp.text)
    assert "Order1" in data["description"]
    assert data["number_of_pieces"] == 2


def test_list_pieces(supply_url):
    url = supply_url + "/pieces"
    resp = requests.get(url)
    assert resp.status_code == 200
    data = json.loads(resp.text)
    assert data[0]["id"] == 1
    assert data[0]["order_id"] == 1
    assert data[1]["id"] == 2
    assert data[1]["order_id"] == 1
