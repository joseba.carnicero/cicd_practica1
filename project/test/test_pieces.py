import json


def test_single_piece(test_app, test_database, add_order, add_user):
    user = add_user(name="Joseba", surname="Carnicero", address="Bergara")
    order = add_order(
        description="New order created from REST API",
        number_of_pieces=2,
        user_id=user.id,
    )
    client = test_app.test_client()
    resp = client.get(f"/pieces/1")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["id"] == 1
    assert data["order_id"] == order.id


def test_all_pieces(test_app, test_database, add_order, add_user):
    user = add_user(name="Joseba", surname="Carnicero", address="Bergara")
    order = add_order(
        description="New order created from REST API",
        number_of_pieces=3,
        user_id=user.id,
    )
    client = test_app.test_client()
    resp = client.get(f"/pieces")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 3
    assert data[0]["id"] == 1
    assert data[0]["order_id"] == order.id
    assert data[1]["id"] == 2
    assert data[1]["order_id"] == order.id
    assert data[2]["id"] == 3
    assert data[2]["order_id"] == order.id
