import pytest
from project import create_app, db
from project.api.models import Order, Piece, User


@pytest.fixture(scope="module")
def test_app():
    app = create_app()
    app.config.from_object("project.config.TestingConfig")
    app.app_context().push()
    with app.app_context():
        yield app


@pytest.fixture(scope="function")
def test_database():
    db.create_all()
    yield db
    db.session.remove()
    db.drop_all()


@pytest.fixture(scope="function")
def add_order():
    def _add_order(description, number_of_pieces, user_id):
        order = Order(
            description=description, number_of_pieces=number_of_pieces, user_id=user_id
        )
        db.session.add(order)
        for i in range(order.number_of_pieces):
            piece = Piece()
            piece.order = order
            db.session.add(piece)
        db.session.commit()
        return order

    return _add_order


@pytest.fixture(scope="function")
def add_user():
    def _add_user(name, surname, address):
        user = User(name=name, surname=surname, address=address)
        db.session.add(user)
        db.session.commit()
        return user

    return _add_user
