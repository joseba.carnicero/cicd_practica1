import json


def test_create_order(test_app, test_database, add_user):
    client = test_app.test_client()
    user = add_user(name="Joseba", surname="Carnicero", address="Bergara")
    resp = client.post(
        "/orders",
        data=json.dumps(
            {
                "description": "New order created from REST API",
                "number_of_pieces": 5,
                "user_id": user.id,
            }
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 201
    assert "Order 1 was added!" in data["message"]


def test_single_order(test_app, test_database, add_order, add_user):
    user = add_user(name="Joseba", surname="Carnicero", address="Bergara")
    order = add_order(
        description="New order created from REST API",
        number_of_pieces=2,
        user_id=user.id,
    )
    client = test_app.test_client()
    resp = client.get(f"/orders/{order.id}")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert "New order created from REST API" in data["description"]
    assert data["id"] == order.id
    assert data["number_of_pieces"] == order.number_of_pieces


def test_all_orders(test_app, test_database, add_order, add_user):
    user = add_user(name="Joseba", surname="Carnicero", address="Bergara")
    add_order(description="Order1", number_of_pieces=2, user_id=user.id)
    add_order(description="Order2", number_of_pieces=4, user_id=user.id)
    client = test_app.test_client()
    resp = client.get(f"/orders")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 2
    assert "Order1" in data[0]["description"]
    assert data[0]["number_of_pieces"] == 2
    assert "Order2" in data[1]["description"]
    assert data[1]["number_of_pieces"] == 4
