import json


def test_add_user(test_app, test_database):
    client = test_app.test_client()
    resp = client.post(
        "/users",
        data=json.dumps(
            {"name": "Joseba", "surname": "Carnicero", "address": "Bergara"}
        ),
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 201
    assert "User Joseba was added!" in data["message"]


def test_single_user(test_app, test_database, add_user):
    user = add_user(name="Joseba", surname="Carnicero", address="Bergara")
    client = test_app.test_client()
    resp = client.get(f"/users/{user.id}")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["id"] == user.id
    assert "Joseba" in data["name"]
    assert "Carnicero" in data["surname"]
    assert "Bergara" in data["address"]


def test_all_users(test_app, test_database, add_user):
    add_user(name="Joseba", surname="Carnicero", address="Bergara")
    add_user(name="Perico", surname="Palotes", address="Arrasate")
    client = test_app.test_client()
    resp = client.get(f"/users")
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 2
    assert "Joseba" in data[0]["name"]
    assert "Perico" in data[1]["name"]
