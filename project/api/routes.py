# project/api/routes.py
from flask import Blueprint, request
from flask_restplus import Api, Resource, fields
from project.api.services import (
    add_order,
    get_all_orders,
    get_order_by_id,
    get_all_pieces,
    get_piece_by_id,
    add_user,
    get_all_users,
    get_user_by_id,
)

# my_machine = Machine()

routes_blueprint = Blueprint("users", __name__)
api = Api(routes_blueprint)


order = api.model(
    "Order",
    {
        "id": fields.Integer(readOnly=True),
        "description": fields.String(required=True),
        "number_of_pieces": fields.Integer(required=True),
        "user_id": fields.Integer(required=True),
        "status": fields.String,
        "creation_date": fields.DateTime,
        "update_date": fields.DateTime,
    },
)


class OrdersList(Resource):
    @api.expect(order, validate=True)
    def post(self):
        post_data = request.get_json()
        description = post_data.get("description")
        number_of_pieces = post_data.get("number_of_pieces")
        user_id = post_data.get("user_id")
        response_object = {}
        new_order = add_order(description, number_of_pieces, user_id)
        # my_machine.add_pieces_to_queue(new_order.pieces)
        response_object["message"] = f"Order {new_order.id} was added!"
        return response_object, 201

    @api.marshal_with(order, as_list=True)
    def get(self):
        return get_all_orders(), 200


api.add_resource(OrdersList, "/orders")


class Orders(Resource):
    @api.marshal_with(order)
    def get(self, order_id):
        order = get_order_by_id(order_id)
        if not order:
            api.abort(404, f"Order {order_id} does not exist")
        return order, 200

    # def delete(self, order_id):
    #     response_object = {}
    #     order = get_order_by_id(order_id)
    #     if not order:
    #         api.abort(404, f"Order {order_id} does not exist")
    #     delete_order(order)
    #     response_object["message"] = f"{order_id} was removed!"
    #     return response_object, 200


api.add_resource(Orders, "/orders/<int:order_id>")

piece = api.model(
    "Piece",
    {
        "id": fields.Integer(readOnly=True),
        "manufacturing_date": fields.String,
        "status": fields.String,
        "order_id": fields.Integer,
        "creation_date": fields.DateTime,
        "update_date": fields.DateTime,
    },
)


class PiecesList(Resource):
    @api.marshal_with(piece, as_list=True)
    def get(self):
        return get_all_pieces(), 200


api.add_resource(PiecesList, "/pieces")


class Pieces(Resource):
    @api.marshal_with(piece)
    def get(self, piece_id):
        piece = get_piece_by_id(piece_id)
        if not piece:
            api.abort(404, f"Piece {piece_id} does not exist")
        return piece, 200


api.add_resource(Pieces, "/pieces/<int:piece_id>")


user = api.model(
    "User",
    {
        "id": fields.Integer(readOnly=True),
        "name": fields.String(required=True),
        "surname": fields.String(required=True),
        "address": fields.String(required=True),
        "creation_date": fields.DateTime,
        "update_date": fields.DateTime,
    },
)


class UsersList(Resource):
    @api.expect(user, validate=True)
    def post(self):
        post_data = request.get_json()
        name = post_data.get("name")
        surname = post_data.get("surname")
        address = post_data.get("address")
        new_user = add_user(name=name, surname=surname, address=address)
        response_object = {"message": f"User {new_user.name} was added!"}
        return response_object, 201

    @api.marshal_with(user, as_list=True)
    def get(self):
        return get_all_users(), 200


api.add_resource(UsersList, "/users")


class Users(Resource):
    @api.marshal_with(user)
    def get(self, user_id):
        user = get_user_by_id(user_id)
        if not user:
            api.abort(404, f"User {user_id} does not exist")
        return user, 200


api.add_resource(Users, "/users/<int:user_id>")
