# models.py
from sqlalchemy.sql import func
from project import db
from sqlalchemy.orm import relationship


class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128), nullable=False)
    surname = db.Column(db.String(128), nullable=False)
    address = db.Column(db.String(128), nullable=False)
    creation_date = db.Column(db.DateTime(timezone=True), server_default=func.now())
    update_date = db.Column(
        db.DateTime, nullable=False, server_default=func.now(), onupdate=func.now()
    )

    orders = relationship("Order", lazy="joined")

    def __init__(self, name, surname, address):
        self.name = name
        self.surname = surname
        self.address = address


class Order(db.Model):
    STATUS_CREATED = "Created"
    STATUS_FINISHED = "Finished"

    __tablename__ = "order"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    number_of_pieces = db.Column(db.Integer, nullable=False)
    description = db.Column(db.String(128), nullable=False, default="No description")
    status = db.Column(db.String(128), nullable=False, default=STATUS_CREATED)
    creation_date = db.Column(db.DateTime(timezone=True), server_default=func.now())
    update_date = db.Column(
        db.DateTime, nullable=False, server_default=func.now(), onupdate=func.now()
    )
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))

    user = relationship("User", backref="piece")
    pieces = relationship("Piece", lazy="joined")

    def __init__(self, number_of_pieces, description, user_id):
        self.number_of_pieces = number_of_pieces
        self.description = description
        self.user_id = user_id


class Piece(db.Model):
    STATUS_CREATED = "Created"
    STATUS_CANCELLED = "Cancelled"
    STATUS_QUEUED = "Queued"
    STATUS_MANUFACTURING = "Manufacturing"
    STATUS_MANUFACTURED = "Manufactured"

    __tablename__ = "piece"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    manufacturing_date = db.Column(db.DateTime(timezone=True), server_default=None)
    status = db.Column(db.String(128), default=STATUS_QUEUED)
    creation_date = db.Column(db.DateTime(timezone=True), server_default=func.now())
    update_date = db.Column(
        db.DateTime, nullable=False, server_default=func.now(), onupdate=func.now()
    )
    order_id = db.Column(db.Integer, db.ForeignKey(Order.id))

    order = relationship("Order", backref="piece")
