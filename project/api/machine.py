from random import randint
from time import sleep
from collections import deque
from .models import Piece, Order
from threading import Thread, Lock, Event
import sqlalchemy
from project.api.services import (
    get_piece_by_status,
    get_pieces_by_status,
    get_piece_by_id,
    change_order_status,
    change_piece_status,
)


class Machine(Thread):
    STATUS_WAITING = "Waiting"
    STATUS_CHANGING_PIECE = "Changing Piece"
    STATUS_WORKING = "Working"
    __stauslock__ = Lock()
    thread_session = None

    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.queue = deque([])
        self.working_piece = None
        self.status = Machine.STATUS_WAITING
        self.instance = self
        self.queue_not_empty_event = Event()
        self.reload_pieces_at_startup()
        self.start()

    def reload_pieces_at_startup(self):
        try:
            manufacturing_piece = get_piece_by_status(status=Piece.STATUS_MANUFACTURING)
            if manufacturing_piece:
                self.add_piece_to_queue(manufacturing_piece)

            queued_pieces = get_pieces_by_status(status=Piece.STATUS_QUEUED)
            if queued_pieces:
                self.add_pieces_to_queue(queued_pieces)
        except (sqlalchemy.exc.ProgrammingError, sqlalchemy.exc.OperationalError):
            print(
                "Error getting Queued/Manufacturing Pieces at startup. It may be the first execution"
            )

    def run(self):
        while True:
            self.queue_not_empty_event.wait()
            # print("Thread notified that queue is not empty.")

            while self.queue.__len__() > 0:
                self.instance.create_piece()

            self.queue_not_empty_event.clear()
            # print("Lock thread because query is empty.")

            self.instance.status = Machine.STATUS_WAITING

    def create_piece(self):
        # Get piece from queue
        piece_id = self.queue.popleft()

        # Machine and piece status updated during manufacturing
        self.working_piece = get_piece_by_id(piece_id=piece_id)
        print(f"Working on piece {self.working_piece.id}")

        # Machine and piece status updated before manufacturing
        self.working_piece_to_manufacturing()

        # Simulate piece is being manufactured
        sleep(randint(5, 20))

        # Machine and piece status updated after manufacturing
        self.working_piece_to_finished()

        self.working_piece = None

    def working_piece_to_manufacturing(self):
        self.status = Machine.STATUS_WORKING
        # self.working_piece.status = Piece.STATUS_MANUFACTURING
        change_piece_status(
            piece_id=self.working_piece.id, status=Piece.STATUS_MANUFACTURING
        )

    def working_piece_to_finished(self):
        self.instance.status = Machine.STATUS_CHANGING_PIECE
        # self.working_piece.status = Piece.STATUS_MANUFACTURED
        change_piece_status(
            piece_id=self.working_piece.id, status=Piece.STATUS_MANUFACTURED
        )

        order_finished = True
        for piece in self.working_piece.order.pieces:
            if piece.status != Piece.STATUS_MANUFACTURED:
                order_finished = False
        if order_finished:
            # self.working_piece.order.status = Order.STATUS_FINISHED
            change_order_status(
                order_id=self.working_piece.order.id, status=Order.STATUS_FINISHED
            )

    def add_pieces_to_queue(self, pieces):
        for piece in pieces:
            self.add_piece_to_queue(piece)

    def add_piece_to_queue(self, piece):
        self.queue.append(piece.id)
        # piece.status = Piece.STATUS_QUEUED
        change_piece_status(piece_id=piece.id, status=Piece.STATUS_QUEUED)
        print(f"Adding piece to queue: {piece.id}")
        self.queue_not_empty_event.set()

    def remove_pieces_from_queue(self, pieces):
        for piece in pieces:
            if piece.status == Piece.STATUS_QUEUED:
                self.queue.remove(piece.id)
                # piece.status = Piece.STATUS_CANCELLED
                change_piece_status(
                    piece_id=piece.piece_id, status=Piece.STATUS_CANCELLED
                )
