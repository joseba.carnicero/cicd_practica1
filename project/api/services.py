from project import db
from project.api.models import Order, Piece, User


def get_all_orders():
    return Order.query.all()


def get_order_by_id(order_id):
    return Order.query.filter_by(id=order_id).first()


def add_order(description, number_of_pieces, user_id):
    order = Order(
        description=description, number_of_pieces=number_of_pieces, user_id=user_id
    )
    db.session.add(order)
    for i in range(order.number_of_pieces):
        piece = Piece()
        piece.order = order
        db.session.add(piece)
    db.session.commit()
    return order


def delete_order(order):
    db.session.delete(order)
    db.session.commit()
    return order


def get_all_pieces():
    return Piece.query.all()


def get_piece_by_id(piece_id):
    return Piece.query.filter_by(id=piece_id).first()


def get_pieces_by_status(status):
    return Piece.query.filter_by(status=status).all()


def get_piece_by_status(status):
    return Piece.query.filter_by(status=status).first()


def change_order_status(order_id, status):
    order = Order.query.filter_by(id=order_id).first()
    order.status = status
    db.session.commit()


def change_piece_status(piece_id, status):
    piece = Piece.query.filter_by(id=piece_id).first()
    piece.status = status
    db.session.commit()


def add_user(name, surname, address):
    user = User(name=name, surname=surname, address=address)
    db.session.add(user)
    db.session.commit()
    return user


def get_all_users():
    return User.query.order_by(User.id).all()


def get_user_by_id(user_id):
    return User.query.filter_by(id=user_id).first()
