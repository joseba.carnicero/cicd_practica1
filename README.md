# P1 - Validated Container Delivery
Este repositorio contiene la Práctica 1º denimonada Validated Container Delivery de la asignatura Integración y Despliegue Continuo del master Análisis de datos, Ciberseguridad y Computación en la nube.

La práctica tiene por objetivo ser capaz de diseñar e implementar un pipeline CI para el sistema de pedidos Order.

Para la entrega del producto final se hayan definidos los siguientes requerimientos:
- Desarrollar un test suite de pruebas unitarias el que se proporcione una cobertura mínima del 20%
- Desarrollar un Smoke Test Suite (STS)  mínimo para el proyecto.
- Añadir una nueva funcionalidad con la metodología TDD
- Crear un pipeline CI de automatización de build y tests, dando como resultado unos contenedores desplegables
- Realizar despliegue manual en el entorno Stage mediante SSH
- Emplear una metodología ágil para la gestión de tareas de la práctica

## Versiones

Con el desarrollo de la práctica se irán publicando nuevas release del proyecto. A continuación las versiones liberadas y sus principales características:

**[v1.0]** - *04/04/2021*
- Refactorización de la aplicación monolítica base.
- Creación de tests unitarios.
- Creación de un Smoke Test Suite.
- Creación de un entorno CI y su pipeline automático.
- Creación de un entorno y scripts de despliegue de la aplicación.

**[v2.0]** - *04/04/2021*
- Desarrollo del servicio Users usando la metodología TDD.

## Documentación

Los detalles de las especificaciones, diseño y evidencias del desarrollo de la práctica quedan recogidos en la documentación complementaria [DOC.md](doc/DOC.md).

## Autores

Joseba Carnicero Padilla – [joseba.carnicero@alumni.mondragon.edu](mailto:joseba.carnicero@alumni.mondragon.edu)

##  Licencia

Este proyecto está bajo Licencia [MIT License](https://choosealicense.com/licenses/mit/). Ver documento [LICENSE.md](LICENSE.md) adjunto en el proyecto para más detalles.
