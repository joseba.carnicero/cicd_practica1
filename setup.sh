#!/bin/bash
sudo yum install -y docker
sudo usermod -a -G docker ec2-user
sudo service docker start
sudo systemctl enable docker  
echo "Docker instalado correctamente"
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
echo "Docker-compose instalado correctamente"