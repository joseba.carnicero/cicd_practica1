#!/bin/bash

echo "Waiting for postgress...."

while ! nc -z db 5432; do
    sleep 0.1
done


echo "PostgresSQL started"

python manage.py run -h 0.0.0.0

