# P1 - Doc

En este documento se recogen la definición y evidencias de la Práctica 1º denominda Validated Container Delivery de la asignatura Integración y Despliegue Continuo del master Análisis de datis, Ciberseguridad y Computación en la nube.

## Servicio Orders

Se tratra de un servicio que gestiona la creación de órdenes y la fabricación de piezas. Cuando se hace una petición se crea una orden con una cantidad de piezas a fabricar y se notifica al servicio Machine para crearlas. Se ha añadido un servicio de healthcheck de la aplicación llamado Ping.

| Endpoint | Método HTTP  | Método CRUD  | Resultado |
|---|---|---|---|
| /ping | GET | - | responde con mensaje "success" |
| /orders | POST | CREATE | crea la orden con los parámetros que se le pasa |
| /orders | GET | READ | muestra todas las órdenes creadas |
| /orders/:id | GET | READ | muestra la orden con el identificador especificado |
| /pieces | GET | READ | muestra todas la piezas creadas |
| /pieces/:id | GET | READ | muestra la pieza con el identificador especificado |

Todos los endpoints escuchan en el puerto 5000.

## Servicio Users

Se tratra de un servicio que gestiona el registro de usuarios. Cada ordern creada es asignada a un usuario por lo que se ha tenido que refactorizar tanto el servicio Orders, para mantener la relación, como sus testeos. 

| Endpoint | Método HTTP  | Método CRUD  | Resultado |
|---|---|---|---|
| /users | POST | CREATE | crea el usuario con los parámetros que se le pasa |
| /users | GET | READ | muestra todas los usuarios registrados |
| /users/:id | GET | READ | muestra el usuario con el identificador especificado |

Todos los endpoints escuchan en el puerto 5000.

Este servicio se ha desarrollado suiguiendo la metodología TDD:

1. Escribir el Test
2. Ejecutar el Test y fallar (ESTADO ROJO)
3. Escribir el mínimo código posible para pasar el test(ESTADO VERDE)
4. Refactorizar el código si se ve necesario

## Test unitarios

Se han testeado varios endpoints individualmente en un entorno de testeos:

- Funcionalidad Ping: /ping
- Funcionalidad Orders: /orders (POST), /orders (GET), /orders/:id
- Funcionalidad Pieces: /pieces, /pieces/:id
- Funcionalidad Users: /users (POST), /users (GET), /users/:id

Para los testeos de Users se ha seguido el siguiente desarrollo:

- Primero, se ha creado el test de creación de un usuario y se ha ejecutado el test, haciendolo fallar.

<p align="center">
  <img src="images/TDD_post_user.PNG">
</p>

<p align="center">
  <img src="images/TDD_post_user2.PNG">
</p>

- Luego, desarrollado el modelo y el endpoint y se ha ejecutado el test de nuevo. Esta vez pasar correctamente.

<p align="center">
  <img src="images/TDD_post_user_OK.PNG">
</p>

- Después, se crean los tests para recibir la información de un usuario y recibir la lista de usuarios.

<p align="center">
  <img src="images/TDD_get_user.PNG">
</p>

<p align="center">
  <img src="images/TDD_get_user2.PNG">
</p>

- Por último, se escriben los endpoints */users* (GET) y */users/:id* y el servicio Users queda desarrollado y testeado.

<p align="center">
  <img src="images/TDD_get_user_OK.PNG">
</p>

Todos los tests han pasado satisfactoriamente y se ha conseguido una covertura de código del 91%.

<p align="center">
  <img src="images/tests_cov2.PNG">
</p>

## Smoke Test Suite

El Smoke Test Suite, o Test de Integración, creado se compone de cinco peticiones que comprueban los endpoints en la aplicación desplegada haciendo llamadas REST. Este test se ejecutará al desplegar la aplicación como última comprobación.

Las tres llamadas que hace son:
- Petición POST para registrar un usuario.
- Petición GET para ver el usuario con un id.
- Petición POST para crear una orden.
- Petición GET para ver la orden con un id.
- Petición GET para ver la lista de piezas que hay creadas.

En el despliegue de prueba el Smoke Test ha pasado correctamente.

<p align="center">
  <img src="images/test_integracion2.PNG">
</p>

## Pipeline CI

El pipeline creado tiene tres fases de la Integración Continua:
- *build*. Se construye la aplicación flask.
- *test*. Se testea la aplicación en un entorno de pruebas.
- *delivery* Se entrega la aplicación en el repositorio.

El pipeline del desarrollo y el merge request tienen que completarse correctamente para poder cerrar cada Milestone.

<p align="center">
  <img src="images/pipelines_milestone2.PNG">
</p>

Estado de los pipelines de los branches en su último *push*.

| Branch | Estado |
|---|---|
| Master | [![pipeline status](https://gitlab.com/joseba.carnicero/cicd_practica1/badges/master/pipeline.svg)](https://gitlab.com/joseba.carnicero/cicd_practica1/commits/master) |
| Dev | [![pipeline status](https://gitlab.com/joseba.carnicero/cicd_practica1/badges/dev/pipeline.svg)](https://gitlab.com/joseba.carnicero/cicd_practica1/commits/dev) |

## Despliegue manual

El despliegue de la aplicación se hace de forma manual en una instancia ec2 de AWS. Las características de la instancia son las siguientes:
- Tipo de intacia: t2.micro
- Ip pública
- Ip elásctica asociada
- Puerto 22 abierto en el Security Group para conexión SSH
- Puerto 5000 abierto en el Security Group para peticiones a la aplicación web

Hay que usar varios comandos y scripts para el despliegue, todo ello con la conexión SSH. 

Primero, hay que instalar *git* en la máquina para poder clonar el repositorio que contiene la aplicación dockerizada.

<kbd>sudo yum install -y git</kbd>

Despues, se clona la aplicación y se concede permiso de ejecución para dos scripts del repositorio.

<kbd>git clone https://CI:CN6uHLcMWqTsN3ba9-xj@gitlab.com/joseba.carnicero/cicd_practica1.git</kbd>

<kbd>cd cicd_practica1</kbd>

<kbd>sudo chmod +x setup.sh</kbd>

<kbd>sudo chmod +x deploy-test.sh</kbd>

Luego, se ejecuta el script [setup.sh](setup.sh) para instalar *docker* y *docker-compose*.

<kbd>./setup.sh</kbd>

Después de ejecutar el script cerrar la conexión SSH e iniciar una nueva. Y por último, usar el script [deploy-test.sh](deploy-test.sh) para desplegar la aplicación y pasar el test de integración.

<kbd>cd cicd_practica1</kbd>

<kbd>./deploy-test.sh</kbd>

Y haciendo una petición al servidor *<ip elástica>:5000/orders* podemos ver la orden creada por el test de integración.

<p align="center">
  <img src="images/test_integracion_resultado_web2.PNG">
</p>

## Restrospective meeting

**Milestone 1**

: ) 
- La división de issues y tareas planteada al inicio de la práctica ha ayudado en desarrollo paso por paso de forma más organizada.

: (
- La estrategia de refactorización de la aplicación inicial no ha sido la correcta. Se desconocía las funciones y características de los módulos y librerías usadas y no se han investagado. Eso ha llevado a gastar mucho tiempo en solucionar errores al refactorizar y a atrasar el desarrollo del resto de issues. Para el próximo proyecto dedicar algo de tiempo en mirar los módulos antes de cambiar el código.

**Milestone 2**

: ) 
- No ha habido mucho que refactorizar del Milestone 1 porque las funcionalidades estaban bien aislados.
- El desarrollo del servicio Users ha sido bastante rápido ya que tiene una estructura parecida al de Users.


## Gestión agile

Para la gestión y desarrollo del software se ha elegido SCRUM. El proyecto se ha dividido en 2 sprints (milestones); la primera destinda a la adecuación del proyecto al precoso CI y la creación del pipeline, y la segunda al desarrollo del nuevo servicio.

Las tareas se colocan en una tabla con los Tags de to-do, doing o closed según su estado en el desarrollo y cuando todas las tareas asignadas a un sprint se cierren, este también se terminará. 

<p align="center">
  <img src="images/development_board.PNG">
</p>

Al final de cada sprint se dedicará un tiempo a hacer el retrospective meeting, y se anotarán las conclusiones razondas.

## Estrategia de branching

Un primer commit del proyecto base en Master. Los commits durante el desarrollo en la rama Dev. Cuando se complete un milestone hacer un último commit en Dev y luego desde gitlab un merge request a master.

## Control de versiones

Para mantener un historial y orden claros de los cambios que se vayan haciendo en el desarrollo del proyecto se va a hacer uso de Git y su funcionalidad de *tags* para versionar.

Para esta gestión de la configuración se va a usar el Versionado Semántico en los *tags* de manera que cada cambio significativo del proyecto estará identificado con una versión *x.x.x.*

La versión se identifica mediante número de la siguiente manera: *Versión mayor.Versión menor.Parche*

<p align="center">
  <img src="images/versions.PNG">
</p>

Cuando se publique una nueva versión en la rama Master en este repositorio, se anotará en el README.md en el apartado Versiones con el siguiente formato:

**[versión]** - *fecha de publicación*
- lista detallada de cambios realizados